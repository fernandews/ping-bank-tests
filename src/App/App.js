import React from 'react';
import {BrowserRouter as Router, Switch } from "react-router-dom";
import './App.css';

function App() {
  return (
    <Switch>
      <Router exact path="/"><Layout/></Router>
    </Switch>
  );
}

export default App;
